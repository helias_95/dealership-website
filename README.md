# CarCar

Team:

- Person 1 - Hector Elias - Service Microservice
- Person 2 - John Gordon - Automobile Sales

## Design

![Project Diagram](Project_Beta_Diagram.png)

## Service microservice

                            ###     SERVICE CRUD DOCUMENTATION    ###


ACTIONS             |   METHOD        |   URL
--------------------|-----------------|------------------------------------------------------------------
List Technicians    |   GET           | http://localhost:8080/api/technicians/
Create a Technician |   POST          | http://localhost:8080/api/technicians/
Delete a Technician |   DELETE        | http://localhost:8080/api/technicians/:id/
List appointments   |   GET           | http://localhost:8080/api/appointments/
Create appointment  |   POST          | http://localhost:8080/api/appointments/
Delete appointment  |   DELETE        | http://localhost:8080/api/appointments/:id/
Set appointment     |   PUT           | http://localhost:8080/api/appointments/:id/cancel/
status to "canceled"|                 |
Set appointment     |   PUT           | http://localhost:8080/api/appointments/:id/finish/
status to "finished |                 |




How to run this project

1. Fork the repo

2. Clone the project

cd <<into your project directory>>



3. Run the following commands in the terminal

docker volume create beta-data
docker compose build
docker compose up



Notes:
When you run docker-compose up and if you're on macOS, you will see a warning about an environment variable named OS being missing. You can safely ignore this.





              ###     B A C K E N D     ###

- Register the microservice app in the project's settings.py
- Create a urls.py file and include it in the project's urls.py

- Create Models: AutomobileVO, Technician, and Appointment
    >AutomobileVO need only include vin and sold as attributes
    >Technician: first_name, last_name, employee_id(must be unique)
    >Appointment: date_time, reason, status, vin (both unique and only 17 char long), customer, vip(boolean, default to false), foreign key to Technician model(one to many relationship).

- Views:
    >Create ModelEncoders for each Model we have created.
    >Create a Views for methods "GET" and "POST" for appointments and technicians
        -inside the appointment post view, make sure to link a technician to an appointment
        -inside the appointment post view, make it so that if a VIN for the car getting the service already exists in our inventory, we mark the appointment to: VIP = True.
    >Create a Delete View function for technician and appointment
    >Create a "PUT" for updating an Appointment's status

- Poller:
    >Create a poller function that will poll for data every 60 seconds from the Inventory API to gather the vin and sold data for our AutomobileVO.



              ####     F R O N T E N D     #####

- Create All Components pertaining to Technicians and Appointments/Services
    - Technician List
        >Lists all technicians
    - New Technician Form
    - (Filtered) Appointment List
        >Shows a list of appointments whose status is "created". Must exclude those with a "finished" or "canceled" status.
        > Include a button on each appointment row that can update and mark an appointment as "finished" or "canceled". Create an async function for a "PUT" method that is invoked upon clicking either button. DO NOT DELETE APPOINTMENTS.
        >In the VIP column, display the Booleans as "Yes" or "No". A simple ternary function would do the trick.
        >will need a chain filter and map function in the return statement
    - Service History
        >Shows a list of ALL appointments, regardless of status.
        >Must have a search bar feature that filters the list by VIN that is has been inputted in the search bar.
            >Search bar is created with an <input> tag. Include a Change Handler that sets a state to the query being made and use that query state in the filter + map chain function in the return statement. Find a way to not make it case sensitive.
        >will need a chain filter and map function in the return statement


    - As each component is created and included in the Root component, create a navlink for the component in the Nav.js file as well.


## Sales microservice

JOHN
Explain your models and integration with the inventory
microservice, here.

STARTING INSTRUCTIONS FROM JAKE:
Set up Insomnia (DONE)
AutomobileVO and other models (DONE)

Start with Poller - working on poller and def get_automobile setup correctly (DONE)

- Need to test if Poller is working properly -> must finish Views to test in sales-api container (DONE)
- Making Views (DONE)
  - all get, post, delete views are completed
  - also included GET specific views

Individual API (DONE)

Frontend Components

- Add a salesperson (DONE)
- List Salespeople (DONE)
- Add a customer (DONE)
- List Customers (DONE)
- Record a New Sale (DONE)
- List all Sales (DONE)
- Salesperson History (DONE)

- Working on Inventory - Manufacturers List (DONE)
- Working on Inventory - Create a Manufacturer (DONE)
- Working on Inventory - Show a list of vehicle models (DONE)

QUESTIONS:
Check Poller and try commented out line

CRUD ROUTES:
Route Name | URL | HTTP Verb | Description

Sales Crud :
List salespeople | GET | http://localhost:8090/api/salespeople/ | list of all salespeople
Create a salesperson | POST | http://localhost:8090/api/salespeople/ | create a salesperson
Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/:id/ | delete specific salesperson
List customers | GET | http://localhost:8090/api/customers/ | list of all customers
Create a customer | POST | http://localhost:8090/api/customers/ | create a customer
Delete a specific customer | DELETE | http://localhost:8090/api/customers/:id/ | delete a specific customer
List sales | GET | http://localhost:8090/api/sales/ | list all sales
Create a sale | POST | http://localhost:8090/api/sales/ | create a sale
Delete a sale | DELETE | http://localhost:8090/api/sales/:id | delete a sale

Inventory CRUD :
List manufacturers | GET | http://localhost:8100/api/manufacturers/ | list of all manufacturers
Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ | create a manufacturer
Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/:id/ | get a specific manufacturer
Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/ | update a specific manufacturer
Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ | delete a specific manufacturer

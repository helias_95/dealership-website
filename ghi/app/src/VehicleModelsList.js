import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";

function VehicleModelsList() {
  const [vehicleModels, setVehicleModels] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");

    if (response.ok) {
      const data = await response.json();
      setVehicleModels(data.models);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map((vehicle) => {
            return (
              <tr key={vehicle.id} value={vehicle.id}>
                <td>{vehicle.name}</td>
                <td>{vehicle.manufacturer.name}</td>
                <td>
                  <img
                    src={vehicle.picture_url}
                    alt="image"
                    height={200}
                    width={300}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default VehicleModelsList;

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './TechList';
import TechForm from './TechForm';
import AppointmentList from './AppointmentList';
import ApptForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';
import NewSalespersonForm from "./NewSalespersonForm";
import SalespeopleList from "./SalespeopleList";
import NewCustomerForm from "./NewCustomerForm";
import CustomerList from "./CustomerList";
import NewSaleForm from "./NewSaleForm";
import AllSalesList from "./AllSalesList";
import SalepersonHistory from "./SalepersonHistory";
import ManufacturersList from "./ManufacturersList";
import ManufacturerForm from "./ManufacturerForm";
import VehicleModelsList from "./VehicleModelsList";
import ModelForm from './ModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={ <MainPage /> } />
          <Route path="technicians" element={ <TechnicianList /> } />
          <Route path="technicians/create" element={ <TechForm /> } />
          <Route path="appointments/" element={ <AppointmentList /> } />
          <Route path="appointments/create" element={ <ApptForm /> } />
          <Route path="appointments/history" element={ <ServiceHistory /> } />
          <Route path="/" element={<MainPage />} />
          <Route path="salesperson/create" element={<NewSalespersonForm />} />
          <Route path="salespeople/" element={<SalespeopleList />} />
          <Route path="customers/create" element={<NewCustomerForm />} />
          <Route path="customers/" element={<CustomerList />} />
          <Route path="sales/create" element={<NewSaleForm />} />
          <Route path="sales/" element={<AllSalesList />} />
          <Route path="sales/history" element={<SalepersonHistory />} />
          <Route path="manufacturers/" element={<ManufacturersList />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />} />
          <Route path="models" element={<VehicleModelsList />} />
          <Route path="models/create" element={ <ModelForm /> } />
          <Route path="automobiles/" element={ <AutomobileList /> } />
          <Route path="automobiles/create" element={ <AutomobileForm /> } />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import React, { useEffect, useState } from "react";

function NewSaleForm() {
  const [automobiles, setAutomobiles] = useState([]);
  const [carVIN, setCarVIN] = useState("");

  const [salespersons, setSalespersons] = useState([]);
  const [salesIndividual, setSalesIndividual] = useState("");

  const [customers, setCustomers] = useState([]);
  const [individualCustomer, setIndividualCustomer] = useState("");

  const [price, setPrice] = useState("");


  // Get automobiles data for dropdown list
  const automobileData = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const autoData = await response.json();
      const unsoldAutos = autoData.autos.filter(automobile => !automobile.sold)
      setAutomobiles(unsoldAutos);
    }
  };


  useEffect(() => {
    automobileData();
  }, []);

  // TESTING
  // console.log(automobiles[1])


  // get salesperson data for dropdown list
  const salespersonsData = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespersons(data.salesperson);
    }
  };

  useEffect(() => {
    salespersonsData();
  }, []);

  // get customers data for dropdown list
  const customersData = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customer);
    }
  };

  useEffect(() => {
    customersData();
  }, []);

  const handleCarVINChange = (event) => {
    setCarVIN(event.target.value);
  };

  const handleSalesIndividualChange = (event) => {
    setSalesIndividual(event.target.value);
  };

  const handleIndividualCustomerChange = (event) => {
    setIndividualCustomer(event.target.value);
  };

  const handlePriceChange = (event) => {
    setPrice(event.target.value);
  };


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.automobile = carVIN;
    data.salesperson = salesIndividual;
    data.customer = individualCustomer;
    data.price = price;

    const saleUrl = `http://localhost:8090/api/sales/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(saleUrl, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      console.log(newSale);

      setCarVIN("");
      setSalesIndividual("");
      setIndividualCustomer("");
      setPrice("");

      await handleSold(carVIN);
    }
  };

  // Get automobile is sold data from Inventory PUT Method and change to true
  // when the SaleForm is submitted
  const handleSold = async (vin) => {

    const data = {};
    data.sold = !false;

    const automobileSoldUrl = `	http://localhost:8100/api/automobiles/${vin}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const soldResponse = await fetch(automobileSoldUrl, fetchConfig);
    if (soldResponse.ok) {
      // console.log("Look here for sold response")
      // console.log(soldResponse)
    } else {
      // console.log("Sold Response Not working")
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">

            {/* AUTOMOBILE */}
            <div className="mb-3">
              <label htmlFor="car" className="form-label">
                Automobile VIN
              </label>
              <select
                value={carVIN}
                onChange={handleCarVINChange}
                name="car"
                required
                id="car"
                className="form-select"
              >
                <option>Choose an Automobile VIN</option>
                {automobiles.map((automobile) => {
                  return (
                    <option key={automobile.id} value={automobile.vin}>
                      {automobile.vin}
                    </option>
                  );
                })}
              </select>
            </div>

            {/* SALESPERSONS */}
            <div className="mb-3">
              <label htmlFor="salesperson" className="form-label">
                Salesperson
              </label>
              <select
                value={salesIndividual}
                onChange={handleSalesIndividualChange}
                name="salesperson"
                required
                id="salesperson"
                className="form-select"
              >
                <option>Choose a salesperson</option>
                {salespersons.map((salesperson) => {
                  return (
                    <option
                      key={salesperson.id}
                      value={salesperson.employee_id}
                    >
                      {salesperson.first_name + " " + salesperson.last_name}
                    </option>
                  );
                })}
              </select>
            </div>

            {/* CUSTOMER */}
            <div className="mb-3">
              <label htmlFor="customer" className="form-label">
                Customer
              </label>
              <select
                value={individualCustomer}
                onChange={handleIndividualCustomerChange}
                name="customer"
                required
                id="customer"
                className="form-select"
              >
                <option>Choose a customer</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.id} value={customer.id}>
                      {customer.first_name + " " + customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                value={price}
                onChange={handlePriceChange}
                placeholder="Price"
                required
                type="text"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button type="submit" className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NewSaleForm;


// Was listed on same line as <button> around line 238 " onClick={() => handleSold(carVIN)} "

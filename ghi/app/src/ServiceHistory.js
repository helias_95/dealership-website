import React, { useEffect, useState } from "react";


function ServiceHistory(){

    const [appointments, setAppointments] = useState([]);
    const [query, setQuery] = useState('');

    const filteredAppointments = appointments.filter(appointment => appointment.vin.toLowerCase().includes(query.toLowerCase()));

    // console.log(appointments.filter(appointment => appointment.vin.includes(query)))
    const getData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            // console.log(data)
            setAppointments(data.appointments);
        }
    }




    useEffect(() => {
        getData();
    }, [])
    return(

        <>
        <h1>Service History</h1>
        <div>
            <input className="form-control my-sm-0" type="search" placeholder="Search by VIN" onChange={(e) => setQuery(e.target.value)}/>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>VIP Service</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Reason</th>
                    <th>Technician</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {/* This is where the map function will go */}
                {filteredAppointments.map((filteredAppointment) => {
                    return(
                        <tr key={filteredAppointment.id}>
                            <td>{filteredAppointment.vin}</td>
                            <td>{(filteredAppointment.vip) ? 'Yes' : 'No'}</td>
                            <td>{filteredAppointment.customer}</td>
                            <td>{new Date(filteredAppointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(filteredAppointment.date_time).toLocaleTimeString()}</td>
                            <td>{filteredAppointment.reason}</td>
                            <td>{filteredAppointment.technician.employee_id}</td>
                            <td>{ filteredAppointment.status }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>

        </>

    );
}

export default ServiceHistory;

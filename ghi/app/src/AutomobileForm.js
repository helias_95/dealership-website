import { useEffect, useState } from 'react';


function AutomobileForm(){
    // color, year, vin, model
    const [models, setModels] = useState([]);
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model, setModel] = useState('');


    const getData = async () => {
        const url = 'http://localhost:8100/api/models';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            // console.log(data)
            setModels(data.models);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const autoUrl = "http://localhost:8100/api/automobiles/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };

        const autoResponse = await fetch(autoUrl, fetchOptions);
        if(autoResponse.ok){
            setColor('');
            setYear('');
            setVin('');
            setModel('');
        }
    }


    useEffect(() =>{
        getData();
    }, [])


    return(
        <>
        <h1>Add an Automobile</h1>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Model</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input value={color} onChange={(e) => setColor(e.target.value)} placeholder="Color" required type="text" id="color" name="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={year} onChange={(e) => setYear(e.target.value)} placeholder="year" required type="text" id="year" name="year" className="form-control"/>
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={vin} onChange={(e) => setVin(e.target.value)} placeholder="vin" required type="text" id="vin" name="vin" className="form-control"/>
                    <label htmlFor="vin">Vin</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="model" className="form-label">Model</label>
                    <select value={model} onChange={(e) => setModel(e.target.value)} required id="model" name="model" className="form-select">
                        <option value="">Choose a Model</option>
                        {models.map(model => {
                            return (
                                <option value={ model.id } key={ model.id } >
                                    { model.name }
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
    );
}

export default AutomobileForm

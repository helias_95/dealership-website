import { useEffect, useState } from 'react';


function ApptForm(){

    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [apptDateTime, setApptDateTime] = useState('');
    // const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [tech, setTech] = useState('');

    const handleChangeVIN = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleChangeCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleChangeDateTime = (event) => {
        const value = event.target.value;
        setApptDateTime(value);
    }

    // const handleChangeTime = (event) => {
    //     const value = event.target.value;
    //     setTime(value);
    // }

    const handleChangeReason = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleChangeTech = (event) => {
        const value = event.target.value;
        setTech(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.vin = vin;
        data.customer = customer;
        data.date_time = apptDateTime;
        // data.date_time = time;
        data.reason = reason;
        data.technician = tech;

        const apptUrl = 'http://localhost:8080/api/appointments/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const apptResponse = await fetch(apptUrl, fetchOptions);
        if(apptResponse.ok){
            setVin('');
            setCustomer('');
            setApptDateTime('');
            // setTime('');
            setReason('');
            setTech('');
        }

    }



    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setTechnicians(data.technicians);
        };
    }



    useEffect(() => {
        getData();
    }, [])

    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Make a Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div  className="form-floating mb-3">
                    <input value={vin}  onChange={handleChangeVIN} placeholder="VIN" required type="text" id="vin" name="vin" className="form-control"/>
                    <label htmlFor="vin">Automobile VIN</label>
                </div>
                <div  className="form-floating mb-3">
                    <input value={customer} onChange={handleChangeCustomer}  placeholder="customer" required type="text" id="customer" name="customer" className="form-control"/>
                    <label htmlFor="customer">Customer Name</label>
                </div>
                <div  className="form-floating mb-3">
                    <input value={apptDateTime} onChange={handleChangeDateTime} placeholder="date_time" required type="datetime-local" id="date_time" name="date_time" className="form-control"/>
                    <label htmlFor="date_time">Date and Time</label>
                </div>
                {/* <div  className="form-floating mb-3">
                    <input value={time} onChange={handleChangeTime} placeholder="date_time" required type="time" id="date_time" name="date_time" className="form-control"/>
                    <label htmlFor="date_time">Time</label>
                </div> */}
                <div  className="form-floating mb-3">
                    <input value={reason} onChange={handleChangeReason} placeholder="reason" required type="text" id="reason" name="reason" className="form-control"/>
                    <label htmlFor="reason">Reason for Service</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="technician" className="form-label">Technician</label>
                    <select value={tech} onChange={handleChangeTech} required id="technician" name="technician" className="form-select">
                        <option value="">Choose a Technician</option>
                        {technicians.map(tech => {
                            return (
                                <option value={ tech.employee_id } key={ tech.id } >
                                    { tech.first_name } { tech.last_name }
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
    );
}

export default ApptForm

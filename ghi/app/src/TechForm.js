import { useEffect, useState } from 'react';


function TechForm(){

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeID, setEmployeeID] = useState('');

    const handleChangeFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleChangeLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleChangeEmployeeID = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;

        // console.log(data)

        const techUrl = 'http://localhost:8080/api/technicians/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const shoeResponse = await fetch(techUrl, fetchOptions);
        if(shoeResponse.ok){
            setFirstName('');
            setLastName('');
            setEmployeeID('');
        }
    }

    return(

        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Technician</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input value={firstName} onChange={handleChangeFirstName} placeholder="First Name" required type="text" id="first_name" name="first_name" className="form-control"/>
                    <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={lastName} onChange={handleChangeLastName} placeholder="Last Name" required type="text" id="last_name" name="last_name" className="form-control"/>
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={employeeID} onChange={handleChangeEmployeeID} placeholder="Employee ID" required type="text" id="employee_id" name="employee_id" className="form-control"/>
                    <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>

    );
}

export default TechForm;
